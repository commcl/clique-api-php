<?php

use GuzzleHttp\Client;


class CliqueInviteApi extends CliqueApiBase {

	public function createInviteTemplate($templateData) {
		$client = new GuzzleHttp\Client();
		$jsonTemplateData = json_encode($templateData);
    	$req = $this->_sendPostRequest("invites/templates", $jsonTemplateData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function($value){
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true);
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["template"];
			},function($reason){
					throw $reason;
			}
		)->wait();
  	}
	
	public function getInviteTemplateByName($template_name) {
		
		if (empty($template_name)) return new Exception("Template name is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("invites/templates/{$template_name}");
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["template"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function updateInviteTemplate($templateData) {
		
		if (empty($templateData) || empty($templateData["name"])) return new Exception("Invalid template data passed.");
		$client = new GuzzleHttp\Client();
		$jsonTemplateData = json_encode($templateData);
		$req = $this->_sendPutRequest("invites/templates/".$templateData["name"], $jsonTemplateData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["template"];
			},function($reason){
				throw $reason;
			}
		)->wait();
  	}

  	public function deleteInviteTemplate($template_name) {
		
		if (empty($template_name)) return new Exception("Template name is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendDeleteRequest("invites/templates/{$template_name}");
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return true;
			},function($reason){
				throw $reason;
			}
		)->wait();
	}


	public function sendInvites($conference_id, $invitesData) {
		if (empty($conference_id) || empty($invitesData)) return new Exception("Invalid data passed.");
		$client = new GuzzleHttp\Client();
		$jsonInvitesData = json_encode($invitesData);
    	$req = $this->_sendPostRequest("invites/{$conference_id}/send", $jsonInvitesData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function($value){
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true);
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["failures"];
			},function($reason){
					throw $reason;
			}
		)->wait();
  	}


}

?>