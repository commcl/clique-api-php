<?php 
use GuzzleHttp\Client;


class CliqueCustomersApi extends CliqueApiBase {

	public function createCustomer($customerData) {
		$client = new GuzzleHttp\Client();
		$jsonCustomerData = json_encode($customerData);
    	$req = $this->_sendPostRequest("customers", $jsonCustomerData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function($value){
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true);
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["customer"];
			},function($reason){
					throw $reason;
			}
		)->wait();
  	}

  	public function getCustomerById($customer_id) {
		
		if (empty($customer_id)) return new Exception("Customer id is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("users/{$customer_id}");
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["user"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function updateCustomer($customerData) {
		
		if (empty($customerData) || empty($customerData["id"])) return new Exception("Invalid customer data passed.");
		$client = new GuzzleHttp\Client();
		$jsonCustomerData = json_encode($customerData);
		$req = $this->_sendPutRequest("customers/".$customerData["id"], $jsonCustomerData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["customer"];
			},function($reason){
				throw $reason;
			}
		)->wait();
  	}
 
  	public function search($searchParams) {
		if (empty($searchParams) || empty(count($searchParams))) return new Exception("searchParams are required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("/customers?".http_build_query($searchParams));
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["result"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

}

?>