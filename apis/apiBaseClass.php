<?php

use GuzzleHttp\Psr7\Request;


spl_autoload_register(function ($name) {
    throw new Exception("Don\'t loaded $name.");
});


class CliqueApiBase {
  
  function __construct($wrapper) {
    $this->wrapper = $wrapper;
    
  }

   protected function _sendPostRequest($path, $body) {
        return new Request('POST', $this->wrapper->apiUrl . $path, [
            'debug' => true, 
            'http_errors'     => false,
            'Content-Type' => 'application/json', 
            'Accept' => 'application/json; charset=utf-8', 
            'Authorization' => "Bearer {$this->wrapper->getNextRequestApiKey()}"            
        ], $body);
  }

  protected function _sendGetRequest($path) {
        return new Request('GET', $this->wrapper->apiUrl . $path, [
            'debug' => true, 
            'http_errors'     => false,
            'Content-Type' => 'application/json', 
            'Accept' => 'application/json; charset=utf-8', 
            'Authorization' => "Bearer {$this->wrapper->getNextRequestApiKey()}"
        ]);
  }


  protected function _sendPutRequest($path, $body) {
    return new Request('PUT', $this->wrapper->apiUrl . $path, [
            'debug' => true, 
            'http_errors'     => false,
            'Content-Type' => 'application/json', 
            'Accept' => 'application/json; charset=utf-8', 
            'Authorization' => "Bearer {$this->wrapper->getNextRequestApiKey()}"
           
    ],$body);
  }

  protected function _sendDeleteRequest($path) {
    return new Request('DELETE', $this->wrapper->apiUrl . $path, [
            'debug' => true, 
            'http_errors'     => false,
            'Content-Type' => 'application/json', 
            'Accept' => 'application/json; charset=utf-8', 
            'Authorization' => "Bearer {$this->wrapper->getNextRequestApiKey()}"
    ]);  
  }
 
}


?>