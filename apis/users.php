<?php 

use GuzzleHttp\Client;


class CliqueUsersApi extends CliqueApiBase {

	public function createUser($userData) {
		$client = new GuzzleHttp\Client();
		$jsonUserData = json_encode($userData);
    	$req = $this->_sendPostRequest("users", $jsonUserData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function($value){
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true);
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["user"];
			},function($reason){
					throw $reason;
			}
		)->wait();
  	}
	  
	public function getUserById($user_id) {
		
		if (empty($user_id)) return new Exception("User uuid is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("users/{$user_id}");
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["user"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function updateUser($userData) {
		
		if (empty($userData) || empty($userData["uuid"])) return new Exception("User id is not set");
		$client = new GuzzleHttp\Client();
		$jsonUserData = json_encode($userData);
		$req = $this->_sendPutRequest("users/".$userData["uuid"], $jsonUserData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["user"];
			},function($reason){
				throw $reason;
			}
		)->wait();
  	}

  	public function getUserToken($user_id) {
		
		if (empty($user_id)) return new Exception("User uuid is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("token/{$user_id}");
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["token"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function getCallHistory($user_id, $top, $skip) {
		
		if (empty($user_id)) return new Exception("User uuid is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("/users/{$user_id}/call_history?top=".($top ?? 10)."&skip=".($skip ?? 0));
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["history"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}
	
	public function search($searchParams) {
		if (empty($searchParams) || empty(count($searchParams))) return new Exception("searchParams are required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("/users?".http_build_query($searchParams));
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["result"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}


}

?>