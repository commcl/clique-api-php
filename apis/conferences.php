<?php

use GuzzleHttp\Client;

class CliqueConferencesApi extends CliqueApiBase {

	public function createConference($confData) {
		$client = new GuzzleHttp\Client();
		$jsonConfData = json_encode($confData);
    	$req = $this->_sendPostRequest("conferences", $jsonConfData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function($value){
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true);
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["conference"];
			},function($reason){
					throw $reason;
			}
		)->wait();
  	}

  	public function getConferenceById($conf_id) {
		
		if (empty($conf_id)) return new Exception("Conf id is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("conferences/{$conf_id}");
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["conference"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function getConferenceParticipantsList($conf_id) {
		
		if (empty($conf_id)) return new Exception("Conf id is required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("conferences/{$conf_id}/user-list-hash");
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["result"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function updateConference($confData) {
		
		if (empty($confData) || empty($confData["id"])) return new Exception("Invalid conference data passed.");
		$client = new GuzzleHttp\Client();
		$jsonConfData = json_encode($confData);
		$req = $this->_sendPutRequest("customers/".$customerData["id"], $jsonConfData);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["conference"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function search($searchParams) {
		if (empty($searchParams) || empty(count($searchParams))) return new Exception("searchParams are required");
		$client = new GuzzleHttp\Client();
		$req = $this->_sendGetRequest("/conferences?".http_build_query($searchParams));
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function ($value) {
				$obj = (string)$value->getBody();
				$body = json_decode($obj, true); 
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["result"];
			},function($reason){
				throw $reason;
			}
		)->wait();
	}

	public function startConference($conf_id) {

		if (empty($conf_id)) return new Exception("Conf id is required");
		$client = new GuzzleHttp\Client();
    	$req = $this->_sendPostRequest("conferences/{$conf_id}/start",[]);
		return $client->sendAsync($req,['exceptions'=>false])->then(
			function($value){
				$obj = (string)$value->getBody();
				$body = json_decode($obj,true);
				if (!$body["ok"]) return $body["error"];
				if ($body["ok"]) return $body["conference"];
			},function($reason){
					throw $reason;
			}
		)->wait();
  	}

}

?>