<?php
require('vendor/autoload.php');
require(__DIR__ . '/apis/apiBaseClass.php');
require(__DIR__ . '/apis/users.php');
require(__DIR__ . '/apis/invite.php');
require(__DIR__ . '/apis/customers.php');
require(__DIR__ . '/apis/conferences.php');


final class CliqueApi {

    function __construct($apiKey, $apiUrl) {
        if (empty($apiKey)) throw new Exception('API key should be provided');

        $this->apiKey = $apiKey;
        $this->apiUrl = $apiUrl ?? 'https://caw.me/api/v2/';
        $this->_nextRequestApiKey = null;

        $this->userApi = new CliqueUsersApi($this);
    }

    public function getNextRequestApiKey() {
        $token = $this->apiKey;
        return $token;
    }

    /* this methods is here for backward compatibility */
    public function newUser($userData) {
        return $this->userApi->createUser($userData);
    }

    public function getUserById($userId) {
        return $this->userApi->getUserById($userId);
    }

    public function updateUser($userData) {
        return $this->userApi->updateUser($userData);
    }

    public function userToken($user_id) {
        return $this->userApi->getUserToken($user_id);
    }

    public function userCallHistory($user_id, $top, $skip) {
        return $this->userApi->getCallHistory($user_id,$top,$skip);
    }

    public function createNewConference($confData) {
        return $this->conferencesApi->createConference($confData);
    }

    public function getConferenceById($confId) {
        return $this->conferencesApi->getConferenceById($confId);
    }

    public function getUserListHash($confId) {
        return $this->conferencesApi->getConferenceParticipantsList($confId);
    }

    /* This apiKey would be used for next request only */
    public function withApiKey($token) {
        $this->_nextRequestApiKey = $token;
        return $this;
    }
}




?>
